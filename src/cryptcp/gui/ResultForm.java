package cryptcp.gui;

import cryptcp.gui.models.Config;
import cryptcp.gui.models.Request;
import javafx.event.ActionEvent;
import javafx.scene.control.TextArea;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

public class ResultForm {
    public TextArea base64;
    private Config config;
    private Request request;

    public void copyToClipboard(ActionEvent actionEvent) throws IOException {
        StringSelection stringSelection = new StringSelection(base64.getText());
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        clipboard.setContents(stringSelection, null);
    }

    public void setConfig(Config config) {
        this.config = config;
    }

    public void setRequest(Request request) throws IOException {
        this.request = request;

        byte[] bytes = Files.readAllBytes(Paths.get(config.getCryptcpFolder() + "\\" + request.getRequestFileName()));
        String reqBase64 = new String(bytes, StandardCharsets.UTF_8);
        base64.setText(reqBase64);
    }
}
