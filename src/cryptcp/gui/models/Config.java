package cryptcp.gui.models;

import java.util.HashMap;

public class Config {
    private String cryptcpFolder;
    private String cryptcpApp;
    private String extentionPostfix;

    private static HashMap<Integer, String> cryptoProviderTable = new HashMap<Integer, String>() {{
        put(75, "GOST R 34.10-2001 Signature with Diffie-Hellman Key Exchange");
        put(80, "GOST R 34.10-2012 (256) Signature with Diffie-Hellman Key Exchange");
        put(81, "GOST R 34.10-2012 (512) Signature with Diffie-Hellman Key Exchange");
    }};

    public static HashMap<Integer, String> getCryptoProviderTable() {
        return cryptoProviderTable;
    }

    public static void setCryptoProviderTable(HashMap<Integer, String> cryptoProviderTable) {
        Config.cryptoProviderTable = cryptoProviderTable;
    }

    public String getCryptcpFolder() {
        return cryptcpFolder;
    }

    public void setCryptcpFolder(String cryptcpFolder) {
        this.cryptcpFolder = cryptcpFolder;
    }

    public String getCryptcpApp() {
        return cryptcpApp;
    }

    public void setCryptcpApp(String cryptcpApp) {
        this.cryptcpApp = cryptcpApp;
    }

    public String getExtentionPostfix() {
        return extentionPostfix;
    }

    public void setExtentionPostfix(String extentionPostfix) {
        this.extentionPostfix = extentionPostfix;
    }
}
