package cryptcp.gui;

import cryptcp.gui.models.Config;
import cryptcp.gui.models.Request;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.charset.Charset;
import java.nio.file.*;
import java.util.*;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MainForm {

    private Logger logger = Logger.getLogger(getClass().getName());

    public VBox subjectPanel;
    public VBox extPanel;

    private Config config = new Config() {{
        setCryptcpFolder("cryptcp");
        setCryptcpApp("cryptcp.x86.exe");
        setExtentionPostfix(".der");
    }};

    private Request request = new Request();

    @FXML
    public void createRequest() throws IOException {

        request = new Request();
        request.setContainerName("TestCert" + UUID.randomUUID().toString().substring(0, 4));
        request.setRequestFileName("request.req");
        request.setSubject(getSubjectString());
        request.setExtensions(getExtensionsList());
        request.setGostId(80);

        File logFile = File.createTempFile("cryptcp", ".log");

        logger.log(Level.INFO, "Generate subject:" + request.getSubject());

        ProcessBuilder pb = new ProcessBuilder(getRunArguments());
        pb.redirectErrorStream(true);
        pb.redirectOutput(logFile);
        pb.directory(new File(config.getCryptcpFolder() + "\\"));

        File executeFile = new File(config.getCryptcpFolder() + "\\" + config.getCryptcpApp());

        if(!executeFile.exists())
        {
            openErrorWindow("Не найден исполняемый файл cryptcp по адрессу\n" +executeFile.getAbsolutePath() );
            return;
        }

        try {
            logger.log(Level.INFO, "Run cryptcp");
            Process start = pb.start();
            start.waitFor();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Failed execute cryptcp app", e);
        }

        byte[] bytes = Files.readAllBytes(logFile.toPath());
        String log = new String(bytes, Charset.forName("CP866"));
        logFile.delete();

        logger.log(Level.INFO, "Cryptcp app output:\n" + log);

        boolean idSuccess = log.contains("[ErrorCode: 0x00000000]");

        if (idSuccess)
            openResultWindow();
        else
            openErrorWindow(log);
    }

    private void openErrorWindow(String error) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);

        alert.setTitle("Ошибка");
        alert.setHeaderText(null);
        alert.setContentText("Не удалось создать запрос:\n" + error);

        alert.showAndWait();
    }

    private void openResultWindow() throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("ResultForm.fxml"));
        Pane root = loader.load();

        Stage stage = new Stage();
        stage.setScene(new Scene(root, 450, 350));
        stage.initModality(Modality.APPLICATION_MODAL);

        ResultForm controller = loader.getController();
        controller.setConfig(config);
        controller.setRequest(request);

        stage.show();
    }

    private String getSubjectString() {
        StringBuilder subject = new StringBuilder();

        for (Node child : subjectPanel.getChildren()) {

            TextField textField = (TextField) child;

            if (textField != null && textField.getText().length() > 0) {
                subject.append(textField.getPromptText() + "=" + textField.getText() + ", ");
            }
        }

        return subject.substring(0, subject.length() - 2);
    }

    private List<String> getExtensionsList() {
        List<String> extensions = new ArrayList<>();

        for (Node child : extPanel.getChildren()) {

            CheckBox checkBox = (CheckBox) child;

            if (checkBox != null && checkBox.isSelected()) {
                extensions.add(checkBox.getAccessibleText());
            }
        }

        return extensions;
    }

    private List<String> getRunArguments() {

        List<String> args = new ArrayList<>();
        args.add(config.getCryptcpFolder() + "\\" + config.getCryptcpApp());

        args.add("-creatrqst");
        args.add(request.getRequestFileName());
        args.add("-provtype");
        args.add(String.valueOf(request.getGostId()));
        args.add("-cont");
        args.add(request.getContainerName());
        args.add("-dn");
        args.add(StringEncodeFix.fix(request.getSubject()));
        args.add("-both");
        args.add("-ku");

        for (String extension : request.getExtensions()) {
            args.add("-ext");
            args.add(extension + config.getExtentionPostfix());
        }

        return args;
    }
}
