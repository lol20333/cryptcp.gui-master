package cryptcp.gui.models;

import java.util.ArrayList;
import java.util.List;

public class Request {
    private String subject;
    private String containerName;
    private String requestFileName;
    private List<String> extensions = new ArrayList<>();
    private int gostId;

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContainerName() {
        return containerName;
    }

    public void setContainerName(String containerName) {
        this.containerName = containerName;
    }

    public String getRequestFileName() {
        return requestFileName;
    }

    public void setRequestFileName(String requestFileName) {
        this.requestFileName = requestFileName;
    }

    public int getGostId() {
        return gostId;
    }

    public void setGostId(int gostId) {
        this.gostId = gostId;
    }

    public List<String> getExtensions() {
        return extensions;
    }

    public void setExtensions(List<String> extensions) {
        this.extensions = extensions;
    }
}
